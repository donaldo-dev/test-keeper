// initial state
const state = () => ({
  items: [],
})

// getters
const getters = {
    all: (state) => {
        return state.items
    },
    getUserRoleById: (state, { id }) => {
        return state.items.find((item) => item.id === id)
    },
}

// actions
const actions = {
    fetchAll ({ commit }) {
        fetch("/mock/user_roles.json")
            .then(response => response.json())
            .then((data) => {
                commit('SET_USER_ROLES', data)
            })
    },
    async createUserRole({ commit }, payload) {
        commit('CREATE_USER_ROLE', payload)
    },
    async updateUserRole({ commit }, payload) {
        commit('UPDATE_USER_ROLE', payload)
    },
    deleteUserRole({ commit }, payload) {
        commit('DELETE_USER_ROLE', payload)
    },
}

// mutations
const mutations = {
    SET_USER_ROLES (state, payload) {
        state.items = payload
    },
    CREATE_USER_ROLE (state, payload) {
        console.log(payload)
        state.items.push(payload)
    },
    UPDATE_USER_ROLE (state, payload) {
        const indexToUpdate = state.items.findIndex((item) => item.id === payload.id)
        if (indexToUpdate > -1) {
            state.items[indexToUpdate] = {...payload}
        }
    },
    DELETE_USER_ROLE (state, { id }) {
        const indexToDelete = state.items.findIndex((item) => item.id === id)

        if (indexToDelete > -1) {
            state.items.splice(indexToDelete, 1)
        }
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}