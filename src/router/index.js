import VueRouter from 'vue-router'
import UserRolesList from '../components/UserRolesList.vue'
import CreateEditUserRole from '../components/CreateEditUserRole.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: UserRolesList
  },
  {
    path: '/create',
    name: 'Create',
    component: CreateEditUserRole
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: CreateEditUserRole
  }
]

const router = new VueRouter({
    routes
})

export default router